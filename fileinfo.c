#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<time.h>
#include<errno.h>
#include<pwd.h>
#include<grp.h>

extern int errno;
void show_stat_info(char*);
int main(int argc,char *argv[])
{
	if(argc != 2)
	{
		fprintf(stderr,"incorrect no. of argumrnts\n");
		exit(1);
	}
	show_stat_info(argv[1]);
	return 0;
}

void show_stat_info(char *fname)
{
	struct stat info;
	int rv = lstat(fname,&info);
	if (rv == -1)
	{
		perror("stat failed");
		exit(1);
	}
	errno = 0;
	struct passwd * pwd = getpwuid(info.st_uid);
	struct group * grp = getgrgid(info.st_gid);
	if(pwd == NULL || grp == NULL)
	{
		if(errno == 0)
		{
			printf("recoed not found \n");
		}
		else
			perror("getpwuid or getgrgid failed");
 
	}

	printf("mode: %o\n", info.st_mode);
	printf("link count: %ld\n", info.st_nlink);
	printf("user: %s\n",pwd->pw_name);
	printf("group: %s\n",grp->gr_name);
	printf("size: %ld\n", info.st_size);
	printf("modtime: %s\n", ctime(&info.st_mtime));
	printf("name: %s\n",fname);
}
